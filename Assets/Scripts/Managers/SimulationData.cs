using UnityEngine;
using System.Collections.Generic;
using System;

public class SimulationData 
{
    public int SimulationOrder;
    public ExperimentalCondition experimentalCondition;
    public List<TrialPhrase> TrialPhraseList;

}

[System.Serializable]
public class ExperimentalCondition
{
    public string name;
    public int idOrder;
    public HapticMode hapticMode;
    public bool HoverHelp;
    public bool PressHelp;

    public ExperimentalCondition(string name, int idOrder, HapticMode hapticMode, bool HoverHelp, bool PressHelp)
    {
        this.name = name;
        this.idOrder = idOrder;
        this.hapticMode = hapticMode;
        this.HoverHelp = HoverHelp;
        this.PressHelp = PressHelp;
    }
}

public class TrialPhrase 
{
    public int PraseID;
    public int OrderPhrase;
    public DateTime DateTimeStartTrial;
    public DateTime DateTimeFirstPressKey;
    public DateTime DateTimeLastPressKey; 
    public string TextToWrite;
    public string WrittenText;
    public bool FirstPressKeyCorrect;
    public int nbcaracters;
    public int Wrongcaracters;
    public float EyeTrackingKeyboardTime;
    public float EyeTrackingScreenTime;
}

public class Phrase
{
    public int id;
    public string text;

    public Phrase()
    {
        id = 0;
        text = "";
    }

    public Phrase(int id, string text)
    {
        this.id = id;
        this.text = text;
    }
} 
