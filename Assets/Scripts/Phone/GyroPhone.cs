using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroPhone : MonoBehaviour
{
    private bool gyroEnabled = false;
    private Quaternion initialRotation;
    public Vector3 customInitialRotationAngles; // Nouvelle variable pour les angles de rotation initiale personnalisée

    private void Start()
    {
        if (SystemInfo.supportsGyroscope)
        {
            Input.gyro.enabled = true;
            gyroEnabled = true;
        }
    }

    private void Update()
    {
        if (gyroEnabled)
        {
            initialRotation = Quaternion.Euler(customInitialRotationAngles); // Utilise les angles de rotation personnalisée lors de l'initialisation

            Quaternion gyroRotation = Input.gyro.attitude;

            Debug.Log(Input.gyro.rotationRate);

            Quaternion unbiasedRotation = Quaternion.Inverse(gyroRotation);

            Quaternion targetRotation = Quaternion.identity;
            targetRotation *= Quaternion.Inverse(unbiasedRotation);

            // Inversion du sens de rotation sur les axes spécifiques
            targetRotation *= Quaternion.AngleAxis(180f, Vector3.left); // Inversion de l'axe X
            targetRotation *= Quaternion.AngleAxis(180f, Vector3.forward); // Inversion de l'axe Z

            if (initialRotation == Quaternion.identity)
            {
                initialRotation = targetRotation;
            }

            transform.rotation = initialRotation * targetRotation; // Utilise la rotation initiale personnalisée
        }
    }
}
