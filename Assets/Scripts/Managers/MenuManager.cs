using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField IF;

    [SerializeField]
    private TMP_Dropdown DD;

    private void Start()
    {
        IF.ActivateInputField();
        DataRecorder.HapticMode = HapticMode.Rien;
    }

    public void DropdownValueChanged()
    {
        switch(DD.value)
        {
            case 0:
                DataRecorder.HapticMode = HapticMode.Rien;          
            break;

            case 1:
                DataRecorder.HapticMode = HapticMode.Lisse;          
            break;

            case 2:
                DataRecorder.HapticMode = HapticMode.Clavier;          
            break;

            case 3:
                DataRecorder.HapticMode = HapticMode.Control;          
            break;
        }
    }

    public void AddSettings()
    {
        if(IF.text != "")
        {
            DataRecorder.NumeroParticipant = Int32.Parse(IF.text);
        }
        else
        {
            return;
        }

        if(DataRecorder.HapticMode == HapticMode.Control)
        {
            SceneManager.LoadScene("ControlGroup");
        }
        else
        {
            SceneManager.LoadScene("HandColor");
        }
    }
}

public enum HapticMode
{
    Rien,
    Lisse,
    Clavier,
    Control,
} 
