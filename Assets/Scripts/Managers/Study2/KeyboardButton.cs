using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardButton : MonoBehaviour
{
    [SerializeField]
    private List<MeshRenderer> Elements = new List<MeshRenderer>();

    public void SetGetKeyDown()
    {
        foreach(var elt in Elements)
        {
            elt.material.color = Study2Manager.Instance.PressColor;
        }
    }

    public void SetGetKeyUp()
    {
        foreach(var elt in Elements)
        {
            elt.material.color = Study2Manager.Instance.NormalColor;
        }
    }

    public void ApplyHighlight()
    {
        foreach(var elt in Elements)
        {
            elt.material.color = Study2Manager.Instance.HightlightColor;
        }
    }

    public void ApplyNormalColor()
    {
        foreach(var elt in Elements)
        {
            elt.material.color = Study2Manager.Instance.NormalColor;
        }
    }
}
