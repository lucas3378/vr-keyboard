using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DemoDRIFT : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField IF;

    public void InsertChar(string c)
    {
        IF.text += c.ToLower();
        IF.caretPosition = IF.text.Length;
    }

    public void InsertSpace()
    {
        IF.text += " ";
        IF.caretPosition = IF.text.Length;
    }
}
