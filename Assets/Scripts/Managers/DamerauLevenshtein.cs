using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamerauLevenshtein
{
    public static float ComputeCER(string s, string t)
    {
        int n = s.Length;
        int m = t.Length;
        int[,] d = new int[n + 1, m + 1];

        // Initialize the matrix
        for (int i = 0; i <= n; i++)
            d[i, 0] = i;
        for (int j = 0; j <= m; j++)
            d[0, j] = j;

        for (int j = 1; j <= m; j++)
        {
            for (int i = 1; i <= n; i++)
            {
                int cost = (s[i - 1] == t[j - 1]) ? 0 : 1;

                d[i, j] = Mathf.Min(Mathf.Min(
                    d[i - 1, j] + 1, // Deletion
                    d[i, j - 1] + 1), // Insertion
                    d[i - 1, j - 1] + cost); // Substitution

                // Transposition
                if (i > 1 && j > 1 && s[i - 1] == t[j - 2] && s[i - 2] == t[j - 1])
                {
                    d[i, j] = Mathf.Min(d[i, j], d[i - 2, j - 2] + cost);
                }
            }
        }

        //return ((float)d[n, m] / s.Length)*100;
        return (float)d[n, m];
    }
}
