using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightVRButton : MonoBehaviour
{
    private KeyboardButton KeyboardButton;

    private void Start()
    {
        KeyboardButton = GetComponentInParent<KeyboardButton>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("ENTER");
        KeyboardButton.ApplyHighlight();
    }

    private void OnTriggerStay(Collider other)
    {
        Debug.Log("STAY");
        KeyboardButton.ApplyHighlight();
    } 
    
    private void OnTriggerExit(Collider other)
    {
        Debug.Log("EXIT");
        KeyboardButton.ApplyNormalColor();
    }
}