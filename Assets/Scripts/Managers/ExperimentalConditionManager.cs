using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperimentalConditionManager
{
    public static List<int> Order1 = new List<int>() { 1, 2, 4, 3};
    public static List<int> Order2 = new List<int>() { 2, 3, 1, 4};   
    public static List<int> Order3 = new List<int>() { 3, 4, 2, 1}; 
    public static List<int> Order4 = new List<int>() { 4, 1, 3, 2}; 


    public static List<int> GetOrderExpe(int participant)
    {
        List<int> result = new List<int>();

        switch (participant % 4)
        {
            case 0:
            result = new List<int>(Order1);
            break;

            case 1:
            result = new List<int>(Order2);
            break;

            case 2:
            result = new List<int>(Order3);
            break;
            
            case 3:
            result = new List<int>(Order4);
            break;
        }

        return result;
    }
}
