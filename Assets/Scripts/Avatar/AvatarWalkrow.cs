using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarWalkrow : MonoBehaviour
{
    [SerializeField]
    private float speed = 4f;

    public Transform target;

    private void Update()
    {

        Vector3 targetDirection = target.position - transform.position;

        // Rotate the forward vector towards the target direction by one step
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, 90.0f * Time.deltaTime, 0.0f);

        // Calculate a rotation a step closer to the target and applies rotation to this object
        transform.rotation = Quaternion.LookRotation(newDirection);

        transform.position = Vector3.MoveTowards(transform.position, target.position, Time.deltaTime * speed);

    }
}
