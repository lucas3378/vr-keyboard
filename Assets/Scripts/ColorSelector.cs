using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ColorSelector : MonoBehaviour
{
    [SerializeField]
    private GameObject[] Hands = new GameObject[2];

    [SerializeField]
    private Slider HandTransparencySlider;

    private void Start()
    {
        HandTransparencySlider.value = Hands[0].GetComponentInChildren<SkinnedMeshRenderer>().material.color.a;
    }

    public void UpdateColor(Image img)
    {
        Array.ForEach(Hands,x => 
        {
            var SkinnedMeshRenderer = x.GetComponentInChildren<SkinnedMeshRenderer>();
            SkinnedMeshRenderer.enabled = true;             
            Color currentColor = SkinnedMeshRenderer.material.color;
            currentColor.r = img.color.r;
            currentColor.g = img.color.g;   
            currentColor.b = img.color.b;         
            SkinnedMeshRenderer.material.color = currentColor;
        });
    }

    public void UpdateTransparence()
    {
        Array.ForEach(Hands,x => 
        {
            var SkinnedMeshRenderer = x.GetComponentInChildren<SkinnedMeshRenderer>();
            SkinnedMeshRenderer.enabled = true;             
            Color currentColor = SkinnedMeshRenderer.material.color;     
            currentColor.a = HandTransparencySlider.value;
            SkinnedMeshRenderer.material.color = currentColor;
        });
    }

    public void StartSimulation()
    {
        Color handColor = Hands[0].GetComponentInChildren<SkinnedMeshRenderer>().material.color;
        DataRecorder.HandsColor = "("+handColor.r.ToString().Replace(',', '.')+"."+handColor.g.ToString().Replace(',', '.')+"."+handColor.b.ToString().Replace(',', '.')+")";
        SceneManager.LoadScene("MainStudy");
    }
}
