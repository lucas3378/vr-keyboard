using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LeftEyeTracking : MonoBehaviour
{
    public static LeftEyeTracking Instance { get; private set; }

    public bool DisplayLine = true;

    public float rayDistance = 1.0f;
    public float rayWidth = 0.01f;

    [SerializeField]
    private LayerMask layersToInclude;

    [SerializeField]
    private Color rayColorDefaultState = Color.yellow;

    [SerializeField]
    private Color rayColorKeyboardState = Color.red; 
    [SerializeField]
    private Color rayColorScreenState = Color.blue;   

    private LineRenderer lineRenderer;

    private void Awake()
    {

        if (Instance)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }

    private void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        SetupRay();
    }

    private void SetupRay()
    {
        lineRenderer.useWorldSpace = false;
        lineRenderer.positionCount = 2;
        lineRenderer.startWidth = rayWidth;
        lineRenderer.endWidth = rayWidth;
        lineRenderer.startColor = rayColorDefaultState;
        lineRenderer.endColor = rayColorKeyboardState;
        lineRenderer.SetPosition(0,transform.position);
        lineRenderer.SetPosition(1, new Vector3(transform.position.x, transform.position.y,
        transform.position.z + rayDistance));
    }

    private void Update()
    {
        lineRenderer.enabled =  DisplayLine;
    }

    private void FixedUpdate()
    {
        /*
        if(!FinalKeyboardManager.Instance.SimulationOnGoing)
            return;
            

        RaycastHit hit;

        Vector3  rayCastDirection = transform.TransformDirection(Vector3.forward) * rayDistance;

        if(Physics.Raycast(transform.position, rayCastDirection, out hit, Mathf.Infinity, layersToInclude))
        {
            GameObject hitObject = hit.collider.gameObject;

            if(hitObject.tag == "Keyboard")
            {
                if(FinalKeyboardManager.Instance.CurrentTrialPhrase != null)
                {
                    KeyboardTouching.Instance.LeftEye = true;
                    lineRenderer.startColor = rayColorKeyboardState;
                    lineRenderer.endColor = rayColorKeyboardState;
                }
            }
            else
            {
                KeyboardTouching.Instance.LeftEye = false;
                lineRenderer.startColor = rayColorDefaultState;
                lineRenderer.endColor = rayColorDefaultState;
            }

            if(hitObject.tag == "Screen")
            {
                if(FinalKeyboardManager.Instance.CurrentTrialPhrase != null)
                {
                    ScreenTouching.Instance.LeftEye = true;                    
                    lineRenderer.startColor = rayColorScreenState;
                    lineRenderer.endColor = rayColorScreenState;           
                }
            }
            else
            {
                ScreenTouching.Instance.LeftEye = false;   
                lineRenderer.startColor = rayColorDefaultState;
                lineRenderer.endColor = rayColorDefaultState;
            }
        }
        else
        {
            KeyboardTouching.Instance.LeftEye = false;
            ScreenTouching.Instance.LeftEye = false;   
            lineRenderer.startColor = rayColorDefaultState;
            lineRenderer.endColor = rayColorDefaultState;
        }
        */
    }
}
