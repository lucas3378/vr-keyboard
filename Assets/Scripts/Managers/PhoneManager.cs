using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PhoneManager : MonoBehaviour
{
    private AudioSource audioSource;

    public float TimeBeforeAlarm = 40f;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(WaitBeforeAlarm());
    }

    public void StartAlaem()
    {
        audioSource.Play();
    }


    public void StopAlarm()
    {
        audioSource.Stop();
    }

    private IEnumerator WaitBeforeAlarm()
    {
        yield return new WaitForSeconds(TimeBeforeAlarm);
        StartAlaem();
        yield return new WaitForSeconds(12f);
        StopAlarm();
    }
}
