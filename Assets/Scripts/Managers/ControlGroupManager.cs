using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;

public class ControlGroupManager : MonoBehaviour
{
    public static ControlGroupManager Instance { get; private set; }

    [Header("UI")]
    public TMP_InputField IF;
    public TMP_Text TextToWrite;
    public TMP_Text ProgressText;
    public GameObject ValidateButton;
    public GameObject MenuEndSimulation;

    [HideInInspector]
    public bool FirstPressKey = false;

    [HideInInspector]
    public int CharacterToWriteIndex = 0;
    
    private List<Phrase> TrainingSentences;
    private List<Phrase> EvaluatedSentences; 
    private List<Phrase> TempEvaluatedSentences;

    private int CurrentTrial = 0;

    public TrialPhrase CurrentTrialPhrase;
    public Phrase CurrentPhrase;

    public SimulationData simulationData;
    private int SimulationOrder = 1;

    private void Awake()
    {

        if (Instance)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }

    private void Start()
    {
        SetupControlGroupSimulation();
    }

    private void Update()
    {
        foreach (KeyCode keyCode in System.Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(keyCode))
            {
                if (IF.interactable)
                {
                    // Vérifie si la touche tapée est une lettre
                    if ((keyCode.ToString().Length == 1 && char.IsLetter(keyCode.ToString()[0])) || keyCode.ToString() == "Space")
                    {
                        if(!FirstPressKey)
                        {
                            FirstPressKey = true;
                                                
                            if(keyCode.ToString().ToLower() == TextToWrite.text[0].ToString().ToLower())
                            {
                                CurrentTrialPhrase.FirstPressKeyCorrect = true;
                            }
                            else
                            {
                                CurrentTrialPhrase.FirstPressKeyCorrect = false;
                            }

                            CurrentTrialPhrase.DateTimeFirstPressKey = System.DateTime.Now;       
                        }

                        CurrentTrialPhrase.DateTimeLastPressKey = System.DateTime.Now;
                        CurrentTrialPhrase.nbcaracters++;  

                        if(CharacterToWriteIndex < TextToWrite.text.Length)
                        {
                            if(TextToWrite.text[CharacterToWriteIndex].ToString().ToLower() == keyCode.ToString().ToLower() || TextToWrite.text[CharacterToWriteIndex].ToString().ToLower() == " ")
                            {
                                CharacterToWriteIndex++;
                            }
                            else
                            {
                                CurrentTrialPhrase.Wrongcaracters++;
                            }
                        }
                    }
                    else
                    {

                    }
                }
            }
        }
    }

    public void SetupControlGroupSimulation()
    {
        ////////// MISE EN PLACE DES PHRASES ////////////////////
        string filePath = "Assets/Resources/Phrases.txt";

        TrainingSentences = new List<Phrase>();
        EvaluatedSentences = new List<Phrase>();

        for(int i = 0 ; i < File.ReadAllLines(filePath).Length ; i++)
        {
            if(i < 2)
            {
                TrainingSentences.Add(new Phrase(0,File.ReadAllLines(filePath)[i]));
            }
            else
            {
                EvaluatedSentences.Add(new Phrase(i-1,File.ReadAllLines(filePath)[i]));
            }
        }

        SetupNewExperimentalCondition();

    }

    public void SetupNewExperimentalCondition()
    {
        simulationData = new SimulationData();
        simulationData.SimulationOrder = SimulationOrder;
        simulationData.experimentalCondition = new ExperimentalCondition("",0,HapticMode.Control,false,false);
        simulationData.TrialPhraseList = new List<TrialPhrase>();       

        CurrentTrial = 0;

        TempEvaluatedSentences = new List<Phrase>(EvaluatedSentences);

        SetupNewTrial();
    }

    public void SetupNewTrial()
    {
        IF.ActivateInputField();
        FirstPressKey = false;

        if(CurrentTrial < 2)
        {
            CurrentPhrase = TrainingSentences[CurrentTrial];
        }
        else
        {
            CurrentPhrase = TempEvaluatedSentences[Random.Range(0,TempEvaluatedSentences.Count)];
        }

        TextToWrite.text = CurrentPhrase.text;
        IF.text = "";
        ProgressText.text = (CurrentTrial+1)+" / "+ (EvaluatedSentences.Count + TrainingSentences.Count);

        TempEvaluatedSentences.Remove(CurrentPhrase);

        CurrentTrialPhrase = new TrialPhrase();
        CurrentTrialPhrase.PraseID = CurrentPhrase.id;
        CurrentTrialPhrase.DateTimeStartTrial = System.DateTime.Now;
        CurrentTrialPhrase.TextToWrite = CurrentPhrase.text;
        CurrentTrialPhrase.nbcaracters = 0;

        CharacterToWriteIndex = 0;
    }

    public void EndTrial()
    {
        CurrentTrialPhrase.WrittenText = IF.text;        

        simulationData.TrialPhraseList.Add(CurrentTrialPhrase);

        if(CurrentTrial < (EvaluatedSentences.Count + TrainingSentences.Count - 1))
        {
            CurrentTrial++;
            SetupNewTrial();
        }
        else
        {
            DataRecorder.DataToSaveControlData(simulationData);
            SetEndSimulation();
        }
    }

    public void SetEndSimulation()
    {
        IF.gameObject.SetActive(false);
        TextToWrite.text = "";
        ProgressText.text = "";
        ValidateButton.SetActive(false);
        MenuEndSimulation.SetActive(true);
    }
}
