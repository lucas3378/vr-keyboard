using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;

public class DataRecorder 
{
    public static int NumeroParticipant;
    public static HapticMode HapticMode;
    public static string HandsColor;
    private static string[] DataTrial = new string[64];


    public static void DataToSaveStudyData(SimulationData data)
    {
        // Numéro de participant
        DataTrial[0] = NumeroParticipant.ToString();

        // id condition
        DataTrial[1] = data.experimentalCondition.name;

        // Ordre de la condition carré latin 
        DataTrial[2] = data.SimulationOrder.ToString(); 

        // Couleur de la main
        DataTrial[3] = HandsColor;

        // Date début de la siulation 
        DataTrial[4] = data.TrialPhraseList[0].DateTimeStartTrial.ToString(); 

        // Temps moyen premier appui 
        DataTrial[5] = AverageTimeFirstPress(data.TrialPhraseList);
        
        // Taux de réussite premier appui 
        DataTrial[6] = FirstPressSuccessRate(data.TrialPhraseList);

        for (int i = 1; i <= 8; i++) // Boucle pour parcourir les phrases 1 et 2
        {
            var phrase = data.TrialPhraseList.Find(x => x.PraseID == i);
    
            if (phrase != null)
            {
                int startIndex = (i - 1) * 7; // Calculer l'index de départ dans le tableau DataTrial
        
                // Temps premier caractère de la phrase
                DataTrial[startIndex + 7] = phrase.DateTimeFirstPressKey.ToString();  

                // Temps dernier caractère de la phrase
                DataTrial[startIndex + 8] = phrase.DateTimeLastPressKey.ToString();    

                // Nombre de caractères saisis dans la phrase
                DataTrial[startIndex + 9] = phrase.nbcaracters.ToString()+" ("+phrase.Wrongcaracters+")";;

                // CPM de la phrase
                DataTrial[startIndex + 10] = PhraseCPM(phrase.DateTimeFirstPressKey, phrase.DateTimeLastPressKey, phrase.nbcaracters); 

                // CER de la phrase
                DataTrial[startIndex + 11] = PhraseCER(phrase);   

                // CER en % de la phrase
                DataTrial[startIndex + 12] = PhraseCERPourcentage(phrase);

                // Pourcentage de temps clavier
                DataTrial[startIndex + 13] = KeyboardLookTime(phrase);

            }
        }

        DataTrial[63] = GetPhraseOrder(data.TrialPhraseList);
        
        WriteCSVStudyData();
    }

    ///// A TESTER //////
    private static string AverageTimeFirstPress(List<TrialPhrase> TrialPhraseList)
    {
        double TotalTime = 0;

        foreach(var phrase in TrialPhraseList)
        {
            TimeSpan difference = phrase.DateTimeFirstPressKey.Subtract(phrase.DateTimeStartTrial);
            TotalTime += difference.TotalSeconds;
        }

        return (TotalTime / (float)TrialPhraseList.Count).ToString("0.00").Replace(',', '.');
    }

    ///// A TESTER //////
    private static string FirstPressSuccessRate(List<TrialPhrase> TrialPhraseList)
    {
        float rate = ((float)TrialPhraseList.FindAll(x => x.FirstPressKeyCorrect == true).Count / TrialPhraseList.Count)*100;
        return rate.ToString("0.00").Replace(',', '.');
    }

    ///// A TESTER //////
    private static string PhraseCPM(DateTime start, DateTime end, int caracters)
    {
        return (caracters / (end - start).TotalSeconds * 60).ToString("0.00").Replace(',', '.');
    }

    ///// A TESTER //////
    private static string PhraseCER(TrialPhrase phrase)
    {
        float CER = DamerauLevenshtein.ComputeCER(phrase.TextToWrite,phrase.WrittenText);
        
        return CER.ToString("0.00").Replace(',', '.');
    }

    private static string PhraseCERPourcentage(TrialPhrase phrase)
    {
        float CER = DamerauLevenshtein.ComputeCER(phrase.TextToWrite,phrase.WrittenText);
        float CERPercentage = (CER / phrase.TextToWrite.Length) * 100;
        return CERPercentage.ToString("0.00").Replace(',', '.');
    }

    private static string KeyboardLookTime(TrialPhrase phrase)
    {
        var value = phrase.EyeTrackingKeyboardTime / (phrase.EyeTrackingKeyboardTime + phrase.EyeTrackingScreenTime)*100;

        return value.ToString("0.00").Replace(',', '.');
    }

    private static string GetPhraseOrder(List<TrialPhrase> TrialPhraseList)
    {
        string result = "";
        TrialPhraseList.Sort((x, y) => x.OrderPhrase.CompareTo(y.OrderPhrase));
        foreach(var phrase in TrialPhraseList)
        {
            result += phrase.PraseID+" / ";
        }
        return result;
    }

    public static void WriteCSVStudyData()
    {
        string path = Application.dataPath;
        path = path.Substring(0, path.LastIndexOf('/'));
        path = path + "/RecordedData/";
        System.IO.Directory.CreateDirectory(path);
        path = path + "StudyData.csv";

        bool fileExists = File.Exists(path);

        using (StreamWriter writer = new StreamWriter(path, true))
        {
            if (!fileExists)
            {
                writer.WriteLine("Numéro de participant,id condition,id order,Couleur de la main,Date début simulation,Temps moyen 1er appui,Taux de réussite premier appui %,Temps premier caractère phrase 1, Temps dernier caractère phrase 1,Nombre de caractère saisie phrase 1,CPM phrase 1,CER phrase 1,CER en % phrase 1, Regard Clavier phrase 1,Temps premier caractère phrase 2, Temps dernier caractère phrase 2,Nombre de caractère saisie phrase 2,CPM phrase 2,CER phrase 2,CER en % phrase 2, Regard Clavier phrase 2,Temps premier caractère phrase 3, Temps dernier caractère phrase 3,Nombre de caractère saisie phrase 3,CPM phrase 3,CER phrase 3,CER en % phrase 3, Regard Clavier phrase 3,Temps premier caractère phrase 4, Temps dernier caractère phrase 4,Nombre de caractère saisie phrase 4,CPM phrase 4,CER phrase 4,CER en % phrase 4, Regard Clavier phrase 4,Temps premier caractère phrase 5, Temps dernier caractère phrase 5,Nombre de caractère saisie phrase 5,CPM phrase 5,CER phrase 5,CER en % phrase 5, Regard Clavier phrase 5,Temps premier caractère phrase 6, Temps dernier caractère phrase 6,Nombre de caractère saisie phrase 6,CPM phrase 6,CER phrase 6,CER en % phrase 6, Regard Clavier phrase 6,Temps premier caractère phrase 7, Temps dernier caractère phrase 7,Nombre de caractère saisie phrase 7,CPM phrase 7,CER phrase 7,CER en % phrase 7, Regard Clavier phrase 7,Temps premier caractère phrase 8, Temps dernier caractère phrase 8,Nombre de caractère saisie phrase 8,CPM phrase 8,CER phrase 8,CER en % phrase 8, Regard Clavier phrase 8,Ordre des phrases"); // Remplacez Colonne1, Colonne2, Colonne3 par vos propres en-têtes
            }

            writer.WriteLine(string.Join(",", DataTrial));
        }
    }

    public static void DataToSaveControlData(SimulationData data)
    {
        // Numéro de participant
        DataTrial[0] = NumeroParticipant.ToString();

        // id condition
        DataTrial[1] = data.experimentalCondition.hapticMode.ToString();

        // Date début de la siulation 
        DataTrial[2] = data.TrialPhraseList[0].DateTimeStartTrial.ToString(); 

        // Temps moyen premier appui 
        DataTrial[3] = AverageTimeFirstPress(data.TrialPhraseList);
        
        // Taux de réussite premier appui 
        DataTrial[4] = FirstPressSuccessRate(data.TrialPhraseList);

        for (int i = 1; i <= 8; i++) // Boucle pour parcourir les phrases 1 et 2
        {
            var phrase = data.TrialPhraseList.Find(x => x.PraseID == i);
    
            if (phrase != null)
            {
                int startIndex = (i - 1) * 6; // Calculer l'index de départ dans le tableau DataTrial
        
                // Temps premier caractère de la phrase
                DataTrial[startIndex + 5] = phrase.DateTimeFirstPressKey.ToString();  

                // Temps dernier caractère de la phrase
                DataTrial[startIndex + 6] = phrase.DateTimeLastPressKey.ToString();    

                // Nombre de caractères saisis dans la phrase
                DataTrial[startIndex + 7] = phrase.nbcaracters.ToString()+" ("+phrase.Wrongcaracters+")";

                // CPM de la phrase
                DataTrial[startIndex + 8] = PhraseCPM(phrase.DateTimeFirstPressKey, phrase.DateTimeLastPressKey, phrase.nbcaracters); 

                // CER de la phrase
                DataTrial[startIndex + 9] = PhraseCER(phrase);   

                // CER en % de la phrase
                DataTrial[startIndex + 10] = PhraseCERPourcentage(phrase);
            }
        }
        
        WriteCSVControlData();
    }

    public static void WriteCSVControlData()
    {
        string path = Application.dataPath;
        path = path.Substring(0, path.LastIndexOf('/'));
        path = path + "/RecordedData/";
        System.IO.Directory.CreateDirectory(path);
        path = path + "ControlData.csv";

        bool fileExists = File.Exists(path);

        using (StreamWriter writer = new StreamWriter(path, true))
        {
            if (!fileExists)
            {
                writer.WriteLine("Numéro de participant,id condition,Date début simulation,Temps moyen 1er appui,Taux de réussite premier appui %,Temps premier caractère phrase 1, Temps dernier caractère phrase 1,Nombre de caractère saisie phrase 1,CPM phrase 1,CER phrase 1,CER en % phrase 1,Temps premier caractère phrase 2, Temps dernier caractère phrase 2,Nombre de caractère saisie phrase 2,CPM phrase 2,CER phrase 2,CER en % phrase 2,Temps premier caractère phrase 3, Temps dernier caractère phrase 3,Nombre de caractère saisie phrase 3,CPM phrase 3,CER phrase 3,CER en % phrase 3,Temps premier caractère phrase 4, Temps dernier caractère phrase 4,Nombre de caractère saisie phrase 4,CPM phrase 4,CER phrase 4,CER en % phrase 4,Temps premier caractère phrase 5, Temps dernier caractère phrase 5,Nombre de caractère saisie phrase 5,CPM phrase 5,CER phrase 5,CER en % phrase 5,Temps premier caractère phrase 6, Temps dernier caractère phrase 6,Nombre de caractère saisie phrase 6,CPM phrase 6,CER phrase 6,CER en % phrase 6,Temps premier caractère phrase 7, Temps dernier caractère phrase 7,Nombre de caractère saisie phrase 7,CPM phrase 7,CER phrase 7,CER en % phrase 7,Temps premier caractère phrase 8, Temps dernier caractère phrase 8,Nombre de caractère saisie phrase 8,CPM phrase 8,CER phrase 8,CER en % phrase 8"); // Remplacez Colonne1, Colonne2, Colonne3 par vos propres en-têtes
            }

            writer.WriteLine(string.Join(",", DataTrial));
        }
    }
}