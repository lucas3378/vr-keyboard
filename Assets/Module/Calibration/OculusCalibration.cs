using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class OculusCalibration : MonoBehaviour
{
    [SerializeField]
    private OVRInputController calibrationController;
    private OVRInput.Controller OVRInputCalibrationController;
    [SerializeField]
    private OVRInputButon calibrationButton;
    private OVRInput.Button OVRInputCalibrationButton;
    [SerializeField]
    private OVRInputButon activationButton;
    private OVRInput.Button OVRInputActivationButton;

    private OVRCameraRig OVRCameraRig;

    [SerializeField]
    private Oculus.Interaction.Input.Visuals.OVRControllerVisual LeftControllerVisual;
    [SerializeField]
    private Oculus.Interaction.Input.Visuals.OVRControllerVisual RightControllerVisual;  

    private Transform OVRControllerVisual;

    public Transform Reference;
    public Transform Desk;

    private void Start()
    {
        if(Desk != null && DataRecorder.HapticMode == HapticMode.Clavier)
        {
            Vector3 newPosition = Desk.transform.position;
            //newPosition.y += 0.02f; 
            newPosition.y += 0.003f;
            Desk.transform.position = newPosition; 
        }

        OVRCameraRig = FindObjectOfType<OVRCameraRig>();
        if (OVRCameraRig == null)
        {
            Debug.LogError("OVRCameraRig is missing in the scene!");
        }

        GetComponent<Renderer>().material.color = Color.yellow;

        switch (calibrationController)
        {
            case OVRInputController.LTouch:
                OVRInputCalibrationController = OVRInput.Controller.LTouch;
                if (LeftControllerVisual.transform != null)
                {
                    OVRControllerVisual = LeftControllerVisual.transform;
                }
                else
                {
                    Debug.LogError("LeftControllerVisual reference is missing");
                }
                break;

            case OVRInputController.RTouch:
                OVRInputCalibrationController = OVRInput.Controller.RTouch;
                if (RightControllerVisual.transform != null)
                {
                    OVRControllerVisual = RightControllerVisual.transform;
                }
                else
                {
                    Debug.LogError("RightControllerVisual reference is missing");
                }
                break;

            default:
                break;
        }

        switch(calibrationButton)
        {
            case OVRInputButon.One:
            OVRInputCalibrationButton = OVRInput.Button.One;
            break;
            case OVRInputButon.Two:
            OVRInputCalibrationButton = OVRInput.Button.Two;
            break;
            case OVRInputButon.PrimaryIndexTrigger:
            OVRInputCalibrationButton = OVRInput.Button.PrimaryIndexTrigger;
            break;
            case OVRInputButon.PrimaryHandTrigger:
            OVRInputCalibrationButton = OVRInput.Button.PrimaryHandTrigger;
            break;
            case OVRInputButon.PrimaryThumbstickUp:
            OVRInputCalibrationButton = OVRInput.Button.PrimaryThumbstickUp;
            break;
            case OVRInputButon.PrimaryThumbstickDown:
            OVRInputCalibrationButton = OVRInput.Button.PrimaryThumbstickDown;
            break;
            case OVRInputButon.PrimaryThumbstickLeft:
            OVRInputCalibrationButton = OVRInput.Button.PrimaryThumbstickLeft;
            break;
            case OVRInputButon.PrimaryThumbstickRight:
            OVRInputCalibrationButton = OVRInput.Button.PrimaryThumbstickRight;
            break;
            default:
            break;
        }

        switch(activationButton)
        {
            case OVRInputButon.One:
            OVRInputActivationButton = OVRInput.Button.One;
            break;
            case OVRInputButon.Two:
            OVRInputActivationButton = OVRInput.Button.Two;
            break;
            case OVRInputButon.PrimaryIndexTrigger:
            OVRInputActivationButton = OVRInput.Button.PrimaryIndexTrigger;
            break;
            case OVRInputButon.PrimaryHandTrigger:
            OVRInputActivationButton = OVRInput.Button.PrimaryHandTrigger;
            break;
            case OVRInputButon.PrimaryThumbstickUp:
            OVRInputActivationButton = OVRInput.Button.PrimaryThumbstickUp;
            break;
            case OVRInputButon.PrimaryThumbstickDown:
            OVRInputActivationButton = OVRInput.Button.PrimaryThumbstickDown;
            break;
            case OVRInputButon.PrimaryThumbstickLeft:
            OVRInputActivationButton = OVRInput.Button.PrimaryThumbstickLeft;
            break;
            case OVRInputButon.PrimaryThumbstickRight:
            OVRInputActivationButton = OVRInput.Button.PrimaryThumbstickRight;
            break;
            default:
            break;
        }
    }

    private void Update()
    {
        if (OVRInput.Get(OVRInputActivationButton, OVRInputCalibrationController))
        {
            GetComponent<Renderer>().enabled = true;

            transform.position = OVRControllerVisual.position;
            transform.localRotation = Quaternion.Euler(0f, OVRControllerVisual.rotation.eulerAngles.y, 0f);

            if(OVRInput.Get(OVRInputCalibrationButton, OVRInputCalibrationController))
            {
                Vector3 diff = Reference.position - transform.position;
                Quaternion diffRotation = Reference.rotation * Quaternion.Inverse(transform.rotation);
                OVRCameraRig.transform.position += diff;
                OVRCameraRig.transform.rotation *= diffRotation;

                if (Mathf.Abs(transform.position.x - Reference.position.x) < 0.0001f &&
                    Mathf.Abs(transform.position.y - Reference.position.y) < 0.0001f &&
                    Mathf.Abs(transform.position.z - Reference.position.z) < 0.0001f &&
                    Mathf.Abs(transform.rotation.y - Reference.rotation.y) < 1f)
                {
                    GetComponent<Renderer>().material.color = Color.green;
                }
                else
                {
                    GetComponent<Renderer>().material.color = Color.yellow;
                }
            }
        }
        else
        {
            GetComponent<Renderer>().enabled = false;
        }
    }
}

/* Possibilité d'ajouter plus de controleurs */
public enum OVRInputController
{
    LTouch,
    RTouch
}

/* Possibilité d'ajouter plus de bouttons */
public enum OVRInputButon
{
    One, 
    Two, 
    PrimaryIndexTrigger, 
    PrimaryHandTrigger, 
    PrimaryThumbstickUp,
    PrimaryThumbstickDown,
    PrimaryThumbstickLeft,
    PrimaryThumbstickRight
}

