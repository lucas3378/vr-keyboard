using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;

public class FinalKeyboardManager : MonoBehaviour
{
    public static FinalKeyboardManager Instance { get; private set; }

    [Header("UI")]
    public TMP_InputField IF;
    public TMP_Text TextToWrite;
    public TMP_Text ProgressText;
    public TMP_Text ProgressCondition;
    public GameObject RedButton;
    public GameObject MenuCalibration;
    public GameObject MenuInputKeyboard;
    public GameObject MenuPostCondition;
    public GameObject MenuEndSimulation;
    public Color NormalColorState;
    public Color HoverColorState;
    public Color SelectColorState;
    private bool MenuPostConditionEnabled = false;

    [HideInInspector]
    public bool FirstPressKey = false;

    [HideInInspector]
    public int CharacterToWriteIndex = 0;

    public List<GameObject> KeyboardButton;
    
    private List<Phrase> TrainingSentences;
    private List<Phrase> EvaluatedSentences; 
    private List<Phrase> TempEvaluatedSentences;

    private int CurrentTrial = 0;

    public TrialPhrase CurrentTrialPhrase;
    public Phrase CurrentPhrase;

    public ExperimentalCondition CurentExperimentalCondition;

    public List<ExperimentalCondition> AllExperimentalCondition = new List<ExperimentalCondition>();
    private List<ExperimentalCondition> ExperimentalConditionList = new List<ExperimentalCondition>();

    public SimulationData simulationData;
    private int SimulationOrder = 1;

    [HideInInspector]
    public bool SimulationOnGoing = false;

    private void Awake()
    {

        if (Instance)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }

    private void Start()
    {

    }

    public void SetupNewSimulation()
    {
        ////////// MISE EN PLACE DES PHRASES ////////////////////
        string filePath = "Assets/Resources/Phrases.txt";

        TrainingSentences = new List<Phrase>();
        EvaluatedSentences = new List<Phrase>();

        for(int i = 0 ; i < File.ReadAllLines(filePath).Length ; i++)
        {
            if(i < 2)
            {
                TrainingSentences.Add(new Phrase(0,File.ReadAllLines(filePath)[i]));
            }
            else
            {
                EvaluatedSentences.Add(new Phrase(i-1,File.ReadAllLines(filePath)[i]));
            }
        }

        //////////// MISE EN PLACE DE L'ORDRE DES CONDITONS /////////
        var orderid = ExperimentalConditionManager.GetOrderExpe(DataRecorder.NumeroParticipant);
        var exptemp = AllExperimentalCondition.FindAll(x => x.hapticMode == DataRecorder.HapticMode);

        for(int i = 0 ; i< orderid.Count ; i++)
        {
            ExperimentalConditionList.Add(exptemp.Find(x => x.idOrder == orderid[i]));
        }
        SetupNewExperimentalCondition();   
    }

    public void SetupNewExperimentalCondition()
    {
        simulationData = new SimulationData();
        simulationData.SimulationOrder = SimulationOrder;
        simulationData.TrialPhraseList = new List<TrialPhrase>();

        CurentExperimentalCondition = ExperimentalConditionList[0];
        simulationData.experimentalCondition = CurentExperimentalCondition;

        ExperimentalConditionList.Remove(CurentExperimentalCondition);
        ProgressCondition.text = (4 - ExperimentalConditionList.Count)+" /4 ("+CurentExperimentalCondition.name+")";            

        foreach(var button in KeyboardButton)
        {
            var interactableColorVisual = button.GetComponentInChildren<Oculus.Interaction.InteractableColorVisual>();

            if(CurentExperimentalCondition.HoverHelp)
            {
                interactableColorVisual._hoverColorState.Color = HoverColorState;
            }
            else
            {
                interactableColorVisual._hoverColorState.Color = NormalColorState;;
            }

            if(CurentExperimentalCondition.PressHelp)
            {
                interactableColorVisual._selectColorState.Color = SelectColorState;
            }
            else
            {
                interactableColorVisual._selectColorState.Color = NormalColorState;                
            }
        }

        CurrentTrial = 0;

        TempEvaluatedSentences = new List<Phrase>(EvaluatedSentences);

        SetupNewTrial();
    }

    public void SetupNewTrial()
    {
        FirstPressKey = false;

        if(CurrentTrial < 2)
        {
            CurrentPhrase = TrainingSentences[CurrentTrial];
        }
        else
        {
            CurrentPhrase = TempEvaluatedSentences[Random.Range(0,TempEvaluatedSentences.Count)];
        }

        TextToWrite.text = CurrentPhrase.text;
        IF.text = "";
        IF.ActivateInputField();
        ProgressText.text = (CurrentTrial+1)+" / "+(EvaluatedSentences.Count+TrainingSentences.Count);

        TempEvaluatedSentences.Remove(CurrentPhrase);

        CurrentTrialPhrase = new TrialPhrase();
        CurrentTrialPhrase.PraseID = CurrentPhrase.id;
        CurrentTrialPhrase.OrderPhrase = CurrentTrial - 1;
        CurrentTrialPhrase.DateTimeStartTrial = System.DateTime.Now;
        CurrentTrialPhrase.TextToWrite = CurrentPhrase.text;
        CurrentTrialPhrase.nbcaracters = 0;
        CharacterToWriteIndex = 0;   
        
        KeyboardTouching.Instance.timeHitKeyboard = 0;
        ScreenTouching.Instance.timeHitScreen = 0; 
    }

    public void EndTrial()
    {
        if(MenuCalibration.activeSelf)
        {
            MenuCalibration.SetActive(false);
            MenuInputKeyboard.SetActive(true);

            LeftEyeTracking.Instance.DisplayLine = false;
            RightEyeTracking.Instance.DisplayLine = false;

            SetupNewSimulation();
            SimulationOnGoing = true;
            return;
        }

        if(!MenuPostConditionEnabled)
        {
            if (IF.text == "")
            {
                return;
            }

            CurrentTrialPhrase.WrittenText = IF.text;

            simulationData.TrialPhraseList.Add(CurrentTrialPhrase);

            if(CurrentTrial < (EvaluatedSentences.Count+TrainingSentences.Count - 1))
            {             
                CurrentTrial++;
                SetupNewTrial();
            }
            else
            {
                DataRecorder.DataToSaveStudyData(simulationData);

                if(ExperimentalConditionList.Count != 0)
                {
                    MenuPostConditionEnabled = true;
                    MenuInputKeyboard.SetActive(false);
                    MenuPostCondition.SetActive(true);
                }
                else
                {
                    SetEndSimulation();
                }
        }
        }
        else
        {
            MenuPostConditionEnabled = false;
            MenuPostCondition.SetActive(false);
            MenuInputKeyboard.SetActive(true);
            SimulationOrder++;
            SetupNewExperimentalCondition();
        }
    }

    public void SetEndSimulation()
    {
        RedButton.SetActive(false);
        MenuInputKeyboard.SetActive(false);
        MenuEndSimulation.SetActive(true);
    }
}
