using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Study2Manager : MonoBehaviour
{
    public static Study2Manager Instance { get; private set; }

    public Color NormalColor;
    public Color PressColor;
    public Color HightlightColor;

    public TMP_InputField IF;

    private void Awake()
    {

        if (Instance)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }

    void Start()
    {
        IF.ActivateInputField();
    }

    void Update()
    {
        foreach (KeyCode keyCode in System.Enum.GetValues(typeof(KeyCode)))
        {
            if (keyCode >= KeyCode.Mouse0 && keyCode <= KeyCode.Mouse6)
                continue;

            if (keyCode >= KeyCode.JoystickButton0 && keyCode <= KeyCode.JoystickButton19)
                continue;    

            if (keyCode >= KeyCode.Joystick1Button0 && keyCode <= KeyCode.Joystick1Button19)
                continue;    

            if (keyCode >= KeyCode.Joystick2Button0 && keyCode <= KeyCode.Joystick2Button19)
                continue; 

            if (Input.GetKeyDown(keyCode))
            {
                Debug.Log("Touche enfoncée: " + keyCode);
                IF.ActivateInputField();
                GameObject.Find(keyCode.ToString())?.GetComponent<KeyboardButton>()?.SetGetKeyDown();
            }

            if (Input.GetKey(keyCode))
            {
                Debug.Log("Touche enfoncée: " + keyCode);
            }

            if (Input.GetKeyUp(keyCode))
            {
                GameObject.Find(keyCode.ToString())?.GetComponent<KeyboardButton>()?.SetGetKeyUp();
            }
        }
    }
}
