using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardTouching : MonoBehaviour
{ 
    public static KeyboardTouching Instance { get; private set; }

    public bool LeftEye = false;
    public bool RightEye = false;

    [HideInInspector]   
    public float timeHitKeyboard = 0f; 

    private void Awake()
    {

        if (Instance)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }

    private void Update()
    {
        if(LeftEye && RightEye)
        {
            timeHitKeyboard += Time.deltaTime;
        }
    }

    public void SetTimeHitKeyboard()
    {
        FinalKeyboardManager.Instance.CurrentTrialPhrase.EyeTrackingKeyboardTime = timeHitKeyboard; 
    }
}
