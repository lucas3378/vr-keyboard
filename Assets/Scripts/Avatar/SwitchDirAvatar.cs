using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchDirAvatar : MonoBehaviour
{
    [SerializeField]
    Transform Target;

    public Transform Avatar1;
    public Transform Avatar2;

    private void Start()
    {
        if (transform.name == "StartWalkAvatar")
        {
            Avatar1.gameObject.SetActive(true);
            Avatar2.gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Avatar")
        {
            if(transform.name == "StartWalkAvatar")
            {
                if (collision.transform.name == Avatar1.name)
                {
                    Avatar1.gameObject.SetActive(false);
                    Avatar2.gameObject.SetActive(true);
                    Avatar2.GetComponent<AvatarWalkrow>().target = Target;
                }
                else
                {
                    Avatar2.gameObject.SetActive(false);
                    Avatar1.gameObject.SetActive(true);
                    Avatar1.GetComponent<AvatarWalkrow>().target = Target;
                }
            }
            else
            {
                if (collision.transform.name == Avatar1.name)
                {
                    Avatar1.GetComponent<AvatarWalkrow>().target = Target;
                }
                else
                {
                    Avatar2.GetComponent<AvatarWalkrow>().target = Target;
                }
            }
        }
    }
}
