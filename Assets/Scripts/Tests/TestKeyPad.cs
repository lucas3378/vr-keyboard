using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TestKeyPad : MonoBehaviour
{
    public void InsertChar(string c)
    {
        if(!FinalKeyboardManager.Instance.SimulationOnGoing)
            return;

        if(!FinalKeyboardManager.Instance.FirstPressKey)
        {
            FinalKeyboardManager.Instance.FirstPressKey = true;
                                                
            if(c.ToLower() == FinalKeyboardManager.Instance.TextToWrite.text[0].ToString().ToLower())
            {
                FinalKeyboardManager.Instance.CurrentTrialPhrase.FirstPressKeyCorrect = true;
            }
            else
            {
                FinalKeyboardManager.Instance.CurrentTrialPhrase.FirstPressKeyCorrect = false;
            }

            FinalKeyboardManager.Instance.CurrentTrialPhrase.DateTimeFirstPressKey = System.DateTime.Now;       
        }

        FinalKeyboardManager.Instance.IF.text += c.ToLower();
        FinalKeyboardManager.Instance.IF.caretPosition = FinalKeyboardManager.Instance.IF.text.Length; 

        FinalKeyboardManager.Instance.CurrentTrialPhrase.DateTimeLastPressKey = System.DateTime.Now;
        FinalKeyboardManager.Instance.CurrentTrialPhrase.nbcaracters++;   

        KeyboardTouching.Instance.SetTimeHitKeyboard();
        ScreenTouching.Instance.SetTimeHitScreen();   

        if(FinalKeyboardManager.Instance.CharacterToWriteIndex < FinalKeyboardManager.Instance.TextToWrite.text.Length)
        {
            if(FinalKeyboardManager.Instance.TextToWrite.text[FinalKeyboardManager.Instance.CharacterToWriteIndex].ToString().ToLower() == c[0].ToString().ToLower())
            {
                FinalKeyboardManager.Instance.CharacterToWriteIndex++;
            }
            else
            {
                FinalKeyboardManager.Instance.CurrentTrialPhrase.Wrongcaracters++;  
            }
        }
    }

    public void InsertSpace()
    {
        if(!FinalKeyboardManager.Instance.SimulationOnGoing)
            return;

        FinalKeyboardManager.Instance.IF.text += " ";
        FinalKeyboardManager.Instance.IF.caretPosition = FinalKeyboardManager.Instance.IF.text.Length;

        if(FinalKeyboardManager.Instance.CharacterToWriteIndex < FinalKeyboardManager.Instance.TextToWrite.text.Length)
        {
            if(FinalKeyboardManager.Instance.TextToWrite.text[FinalKeyboardManager.Instance.CharacterToWriteIndex] == ' ')
            {
                FinalKeyboardManager.Instance.CharacterToWriteIndex++;
            }
            else
            {
                FinalKeyboardManager.Instance.CurrentTrialPhrase.Wrongcaracters++;                  
            }
        }

        FinalKeyboardManager.Instance.CurrentTrialPhrase.nbcaracters++;  
    }
}
