using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenTouching : MonoBehaviour
{
    public static ScreenTouching Instance { get; private set; }

    public bool LeftEye = false;
    public bool RightEye = false;

    [HideInInspector]   
    public float timeHitScreen = 0f; 

    private void Awake()
    {

        if (Instance)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }

    private void Update()
    {
        if(LeftEye && RightEye)
        {
            timeHitScreen += Time.deltaTime;
        }
    }

    public void SetTimeHitScreen()
    {
        FinalKeyboardManager.Instance.CurrentTrialPhrase.EyeTrackingScreenTime = timeHitScreen;  
    }
}
